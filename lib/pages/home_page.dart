import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
 }
class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

  }

  @override
  void dispose(){
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch(state){
      case AppLifecycleState.paused:
      print('APLICACIÓN EN PAUSA');
        break;
      case AppLifecycleState.resumed:
        print('APLICACIÓN EN ESTADO RESUMED');
        break;
      case AppLifecycleState.inactive:
        print('APLICACIÓN INACTIVA');
        break;
      case AppLifecycleState.suspending:
       print('APLICACIÓN SUSPENDIDA'); 
        break;
    }
  }
  @override
  Widget build(BuildContext context) {
   return new Scaffold(
  
   );
  }
}